package testing.net.pages;

import org.openqa.selenium.WebDriver;
import testing.net.model.LetterModel;

public class DraftLettersPage extends BasePageObject{

    private String url   = "https://e.mail.ru/messages/drafts/";

    private String labelEmail = "//div[@class='b-datalist__item__addr' and contains(text(), '%s')]";
    private String labelText  = "//div[@class='b-datalist__item__panel']//div[@class='b-datalist__item__subj']//span[text()='%s']";
    private String labelSubj  = "//div[@class='b-datalist__item__panel']//div[@class='b-datalist__item__subj' and contains(text(),'%s')]";

    public DraftLettersPage(WebDriver driver){
        super(driver);
    }

    public void Open(){
        driver.get(url);
    }

    public boolean isLetterAvalaible(LetterModel letter){
        return  isElementAvailable(String.format(labelEmail,letter.getEmail()))&&
                isElementAvailable(String.format(labelText,letter.getText()))&&
                isElementAvailable(String.format(labelSubj,letter.getSubject()));
    }
}
