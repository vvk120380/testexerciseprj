package testing.net.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

public class BasePageObject {

    protected WebDriver driver;

    public BasePageObject(WebDriver driver){
        this.driver = driver;
    }

    public void inputText(String xpath, String text)
    {
//        WebDriverWait wait = new WebDriverWait(driver, 10);
//        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
        WebElement element = driver.findElement(By.xpath(xpath));
        element.click();
        element.clear();
        element.sendKeys(text);
    }

    public void click(String xpath)
    {
        WebElement element = driver.findElement(By.xpath(xpath));
        element.click();
    }

    public boolean waitForText(String xpath, long l, TimeUnit tu){
        driver.manage().timeouts().implicitlyWait(l, tu);
        return driver.findElement(By.xpath(xpath)).isDisplayed();
    }

    public boolean isElementAvailable(String xpath){
        return driver.findElement(By.xpath(xpath)).isEnabled() || driver.findElement(By.xpath(xpath)).isDisplayed();
    }

    public String getText(String xpath){
        return driver.findElement(By.xpath(xpath)).getText();
    }

}
