package testing.net.pages;

import org.openqa.selenium.*;
import testing.net.model.LetterModel;

import java.util.concurrent.TimeUnit;

public class NewLetterPage extends BasePageObject{

    private String inputTo        = "//textarea[@data-original-name = 'To']";
    private String inputSubj      = "//input[@name = 'Subject']";
    private String btnSaveDraft   = "//span[text()='Сохранить']";
    private String btnSend        = "//span[text()='Отправить']";
    private String labelSaveState = "//a[@href='/messages/drafts' and text() = 'черновиках']";
    private String labelSendState = "//div[@class='message-sent__title']";



    public NewLetterPage(WebDriver driver){
        super(driver);
    }

    private void InputDataToLetterBody(String text){
        driver.switchTo().frame(driver.findElement(By.xpath("//iframe")));
        WebElement body = driver.findElement(By.cssSelector("body"));
        ((JavascriptExecutor)driver).executeScript(String.format("arguments[0].innerHTML = '%s'", text), body);
        driver.switchTo().defaultContent();
    }

    public void fillData(LetterModel letter){
        inputText(inputSubj, letter.getSubject());
        InputDataToLetterBody(letter.getText());
        inputText(inputTo, letter.getEmail());
    }

    public void saveAsDraft(){
        click(btnSaveDraft);
    }

    public boolean isSaveAsDraft(){
        return waitForText(labelSaveState, 10, TimeUnit.SECONDS);
    }

    public boolean isPageOpen() {
        return isElementAvailable(inputTo) &&
               isElementAvailable(inputSubj)&&
               isElementAvailable(btnSaveDraft);
    }

    public void send() {
        click(btnSend);
    }

    public boolean isSend(){
        return waitForText(labelSendState, 10, TimeUnit.SECONDS);
    }

}
