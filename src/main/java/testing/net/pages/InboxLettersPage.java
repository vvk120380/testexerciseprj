package testing.net.pages;

import org.openqa.selenium.WebDriver;
import java.util.concurrent.TimeUnit;

public class InboxLettersPage extends BasePageObject{

    private String url = "https://e.mail.ru/messages/inbox/";
    private String logoutURL = "https://mail.ru/?from=logout";

    private String btnCreate = "//span[text()='Написать письмо']";
    private String btnLogout = "//a[@id='PH_logoutLink']";
    private String labelEmail = "//i[@id='PH_user-email']";

    public InboxLettersPage(WebDriver driver){
        super(driver);
    }

    public NewLetterPage createLetter() {
        click(btnCreate);
        return new NewLetterPage(driver);
    }

    public String getUserEmailAddress() {
        return getText(labelEmail);
    }

    public void open() {
        driver.get(url);
    }

    public boolean logout() {
        click(btnLogout);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return driver.getCurrentUrl().contains(logoutURL);
    }
}
