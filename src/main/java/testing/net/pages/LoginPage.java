package testing.net.pages;
import org.openqa.selenium.*;

public class LoginPage extends BasePageObject{

    private String inputPassword = "//input[@name='Password']";
    private String inputLogin    = "//input[@name='Login']";
    private String btnSubmit     = "//button[@type='submit']";

    public LoginPage(WebDriver driver){
        super(driver);
    }

    public void open(String url){
        driver.get(url);
    }

    public InboxLettersPage login (String login, String password){
        inputText(inputPassword, password);
        inputText(inputLogin, login);
        click(btnSubmit);
        return new InboxLettersPage(driver);
    }
}
