package testing.net.model;

public class LetterModel {

    private String Email;
    private String Subject;
    private String Text;

    public LetterModel(String email, String subj, String text)
    {
        setEmail(email);
        setSubject(subj);
        setText(text);
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }
}
