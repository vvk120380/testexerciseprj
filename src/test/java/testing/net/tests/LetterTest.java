package testing.net.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import testing.net.model.LetterModel;
import testing.net.steps.LetterSteps;
import java.util.concurrent.TimeUnit;
import static org.assertj.core.api.Assertions.assertThat;

public class LetterTest {

    protected static WebDriver driver;
    private LetterModel letter = new LetterModel("scbtester@mail.ru", "Тестовое задание", "Тестовое письмо");
    private String baseURL  = "https://account.mail.ru/login/";
    private String login    = "scbtester";
    private String password = "Admin123";

    @Before
    public void setUp()
    {
        System.setProperty("webdriver.chrome.driver", "c:\\Chromedriver\\2.41\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void draftLeterTest(){
        LetterSteps letterSteps = new LetterSteps(driver);

        assertThat(letterSteps.openMailSiteAndLogin(baseURL, login, password))
                .as("Не удалось авторизоваться в почтовом сервисе")
                .isTrue();

        assertThat(letterSteps.createNewMail())
                .as("Не удалось открыть форму создания нового письма")
                .isTrue();

        assertThat(letterSteps.fillDataAndSaveLetterAsDraft(letter))
                .as("Не удалось сохранить письмо как черновик")
                .isTrue();

        assertThat(letterSteps.openDraftPageAndFindLetter(letter))
                .as("Письмо не сохранилось в папке с черновиками")
                .isTrue();

        assertThat(letterSteps.logout())
                .as("Ошибка выхода из системы")
                .isTrue();
    }

    @Test
    public void sendLeterTest(){
        LetterSteps letterSteps = new LetterSteps(driver);

        assertThat(letterSteps.openMailSiteAndLogin(baseURL, login, password))
                .as("Не удалось авторизоваться в почтовом сервисе")
                .isTrue();

        assertThat(letterSteps.createNewMail())
                .as("Не удалось открыть форму создания нового письма")
                .isTrue();

        assertThat(letterSteps.fillDataAndSendLetter(letter))
                .as("Не удалось отправить письмо")
                .isTrue();

        assertThat(letterSteps.openSendPageAndFindLetter(letter))
                .as("Письмо не отправилось")
                .isTrue();

        assertThat(letterSteps.logout())
                .as("Ошибка выхода из системы")
                .isTrue();
    }

    @After
    public void tearDown()
    {
        driver.quit();
    }
}
