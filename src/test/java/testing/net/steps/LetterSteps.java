package testing.net.steps;

import org.openqa.selenium.WebDriver;
import testing.net.model.LetterModel;
import testing.net.pages.*;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by md.sar.pc6 on 19.10.2018.
 */
public class LetterSteps {

    protected WebDriver driver;
    private InboxLettersPage inboxLettersPage;
    private NewLetterPage newLetterPage;
    private DraftLettersPage draftLettersPage;
    private SendLettersPage sendLettersPage;

    public LetterSteps(WebDriver driver)
    {
        this.driver = driver;
    }

    public boolean openMailSiteAndLogin(String url, String login, String pass)
    {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open(url);
        inboxLettersPage = loginPage.login("scbtester","Admin123");
        return inboxLettersPage.getUserEmailAddress().contains(login);
    }

    public boolean createNewMail() {
        newLetterPage = inboxLettersPage.createLetter();
        return newLetterPage.isPageOpen();
    }

    public boolean fillDataAndSaveLetterAsDraft(LetterModel letter) {
        newLetterPage.fillData(letter);
        newLetterPage.saveAsDraft();
        return newLetterPage.isSaveAsDraft();
    }

    public boolean fillDataAndSendLetter(LetterModel letter) {
        newLetterPage.fillData(letter);
        newLetterPage.send();
        return newLetterPage.isSend();
    }

    public boolean openDraftPageAndFindLetter(LetterModel letter) {
        draftLettersPage = new DraftLettersPage(driver);
        draftLettersPage.Open();
        return draftLettersPage.isLetterAvalaible(letter);
    }

    public boolean openSendPageAndFindLetter(LetterModel letter) {
        sendLettersPage = new SendLettersPage(driver);
        sendLettersPage.Open();
        return sendLettersPage.isLetterAvalaible(letter);
    }

    public boolean logout() {
        inboxLettersPage.open();
        return inboxLettersPage.logout();
    }

}
